# Jokes App
A Node.js application created with Express.js framework for importing Jokes from [JSON](https://github.com/15Dkatz/official_joke_api/blob/master/jokes/index.json).

## Start Guide

### H2 Database
For simplicity and fastness, I've decided to use an in-memory SQL H2 database.
Before shooting the app, it's necessary to build up the database, wich it's very easy:
* Firstly, you need to have Java installed in your computer. If it's not installed, you can download directly from [Java official page](https://www.java.com/es/download/ie_manual.jsp). ***I recommend Java 8 as it is the version that I've been using during the process.***
* Get sure that you have 'java/bin' in your computer path.
* Then, open your system console an go to the root of this project.
* Once on the project root directory, just run the following command:
``` java -cp ./h2-1.4.200.jar org.h2.tools.Server -tcp -tcpAllowOthers -tcpPort 5234 -baseDir ./ -ifNotExists ```
* Now the H2 Database is running and listening. 
    * If an error occurs, maybe the *-tcpPort 5234* is already in use. Find one free and run the command again.

### Node.js App
Once the database is running, we are ready to start the project:
* Install Node.js in your system. You can download it from the [Node.js page](https://nodejs.org/es/). _I used v14 during development, so thats which I recomend to install._
* Then, open your system console an go to the root of this project.
* Before launching, please get sure that the DB configuration (*DB_HOST* and *DB_PORT*) are correct in the project config ('./config/config.env' from the root). *Take special care with this step if you changed the configuration while building up the BD server.* 
* Install all the dependencies by running this command on the project root: 
``` npm install ```
* Run the following command on the project root: 
``` npm start ```
* You should see a message like '_Listening at URL: http://localhost:3000/_'. Open your browser and visit the given URL to get in the app.
