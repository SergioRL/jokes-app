module.exports = class Joke {
    constructor (id, type, setup, punchline) {
        this.id = Number(id);
        this.type = String(type);
        this.setup = String(setup);
        this.punchline = String(punchline);
    }
}