const Joke = require('../model/joke');
const apiClient = require('../client/jokes-client');
const jokeService = require('./joke-service');

module.exports = {
    importAllJokes: () => {
        console.log('LOAD INIT');

        apiClient.getJokesArray().then(
            (jokes) => {

                console.log(`${jokes.length} elements to be saved`);

                jokes.forEach(
                    (joke) => {
                        var jokeEntity = new Joke(joke.id, joke.type, joke.setup, joke.punchline);
                        
                        try {
                            jokeService.saveJoke(jokeEntity);
                        } catch (error){
                            console.error('Joke could not be saved:');
                            console.error(jokeEntity);
                            console.error(error);
                        }
                    }
                );
        
                console.log('LOAD END');
            }  
        );
    }
}