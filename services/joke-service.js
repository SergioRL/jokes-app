const Joke = require('../model/joke');
const jokePersistence = require('../db/joke-persistence');
const schema = require('../db/schema');

module.exports = {
    saveJoke: (joke) => {
        if (joke instanceof Joke) {
            const { error, value } = schema.jokeSchema.validate(joke.body);

            if (error) {
                throw error;
            } else {
                jokePersistence.create(joke)
            }

        } else {
            throw 'The given object is not instance of Joke';
        }
    },
    getAllJokes:  (callback) => {
        return jokePersistence.getAll(
            (jokes) => {
                callback(jokes);
            });
    },
    getRandomJoke: (callback) => {
        return jokePersistence.getFirstRandom(
            (joke) => {
                callback(joke);
            });
    }
    

}