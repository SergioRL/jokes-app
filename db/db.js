const JDBC = require('jdbc');
const jinst = require('jdbc/lib/jinst');
const dotenv = require('dotenv');
const scripts = require('./scripts');

const util = require('util')

if (!jinst.isJvmCreated()) {
    jinst.addOption("-Xrs");
    jinst.setupClasspath(['./h2-1.4.200.jar']);
};

var h2 = new JDBC({
    url: `jdbc:h2:tcp://${process.env.DB_HOST}:${process.env.DB_PORT}/jokes;database_to_lower=true`,
    drivername: 'org.h2.Driver',
    minpoolsize: 1,
    maxpoolsize: 5,
    properties: {
        user: 'SA',
        password: ''
    }
});

var h2Init = false;

function getH2(callback) {
    if (!h2Init)
        h2.initialize((err) => {
            h2Init = true;
            callback(err)
        });
    return callback(null);
};

function queryDB(sql, callback) {
    h2.reserve((err, connobj) => {

        if (err)
            console.log(err);

        if (connobj) {
            h2.release(connobj, () => 
                connobj.conn.createStatement((err, statement) => {

                    if (err) {
                        console.error(err);
                    }

                    if (statement) {
                        if (callback) {
                            statement.executeQuery(
                                sql, (err, result) => {
                                    if(err)
                                        console.error(err);

                                    h2.release(connobj, (err) => {      
                                        if (err)
                                            console.error(err);

                                        callback(result);
                                    })
                                });
                        } else {
                            statement.executeUpdate(sql, (err) => h2.release(connobj, (err) => { if (err) console.error(err) }));
                        }
                    } else {
                        h2.release(connobj, () => {console.error(err)});
                    }
                }))
        }
    });
};

function queryExecuteDB(sql, callback) {
    h2.reserve((err, connobj) => {

        if (err)
            console.log(err);

        if (connobj) {
            h2.release(connobj, () => 
                connobj.conn.createStatement((err, statement) => {

                    if (err) {
                        console.error(err);
                    }

                    if (statement) {
                        if (callback) {
                            statement.execute(
                                sql, (err, result) => {
                                    if(err)
                                        console.error(err);

                                    h2.release(connobj, (err) => {      
                                        if (err)
                                            console.error(err);

                                        callback(result);
                                    })
                                });
                        } else {
                            statement.execute(sql, (err) => h2.release(connobj, (err) => { if (err) console.error(err) }));
                        }
                    } else {
                        h2.release(connobj, () => {console.error(err)});
                    }
                }))
        }
    });
};

module.exports = {
    initialize: function (callback) {
        getH2((err) => {
            queryDB(scripts.initScript);
        });
    },
    getH2: getH2,
    queryDB: queryDB,
    queryExecuteDB: queryExecuteDB
};