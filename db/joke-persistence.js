const db = require('./db');
const dbUtils = require('./db-utils')

module.exports = {
    getAll: function (callback) {
        db.getH2((err) => db.queryDB("SELECT * FROM joke", (result) => {
            result.toObjArray((err, results) => callback(results))
        }));
    },
    getAllId: function (callback) {
        db.getH2((err) => db.queryDB("SELECT id FROM joke", (result) => {
            result.toObjArray((err, results) => callback(results))
        }));
    },
    getById: function (id, callback) {
        db.getH2((err) => db.queryDB(`SELECT * FROM joke WHERE id = ${id}`, (result) => {
            result.toObjArray((err, results) => {
                return (results.length > 0) ? callback(results[0]) : callback(null);
            })
        }));
    },
    getFirstRandom: function (callback) {
        db.getH2((err) => db.queryDB("SELECT * FROM joke ORDER BY RANDOM() LIMIT 1", (result) => {
            result.toObjArray((err, results) => {
                if (err)
                    console.error(err)

                return (results.length > 0) ? callback(results[0]) : callback(null);
            })
        }));
    },
    create: function (joke) {
        db.getH2((err) => {
            columns = Object.keys(joke).join();
            Object.keys(joke).forEach((key) => joke[key] = `'${dbUtils.escapeValue(joke[key])}'`);
            values = Object.values(joke).join();
            db.queryExecuteDB(`INSERT INTO joke (${columns}) VALUES(${values})`);
        });
    },
    update: function (id, joke) {
        db.getH2((err) => {
            keyValues = []
            Object.keys(joke).forEach((key) => keyValues.push(`${key} = '${joke[key]}'`));

            db.queryDB(`UPDATE joke SET ${keyValues.join()} WHERE id = ${id}`);
        });
    },
    delete: function (id) {
        db.getH2((err) => db.queryDB(`DELETE FROM joke WHERE id = ${id}`));
    }
};