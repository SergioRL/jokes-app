module.exports = {
    initScript: `

DROP TABLE IF EXISTS joke;

CREATE TABLE IF NOT EXISTS joke (
    id INT PRIMARY KEY,
    type VARCHAR,
    setup VARCHAR NOT NULL,
    punchline VARCHAR NOT NULL);

`
};