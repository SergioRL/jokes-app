let showingPunchline = false;
let joke;

$(document).ready(function () {

    if (!joke) {
        loadNewJoke();
    }
});

$(document).click(() => onDocumentClick());

onDocumentClick = () => {
    if (showingPunchline) {
        horizontalHide();
        setTimeout(
            () => {
                loadNewJoke()
            }, 500);
    } else {
        showPunchline();
    }
};

loadNewJoke = () => {
    $.ajax({
        method: "GET", 
        url: `/api/joke/random`
    })
    .done(function( msg ) {
        joke = msg;
        initilalizeView();
    });
};

initilalizeView = () => {

    $("#punchline-box").css('left', '100%');
    $("#setup-box").css('left', '-100%').animate({left: '0%'});

    $("#setup-line").html(joke.setup);
    $("#punchline").html(joke.punchline);

    showingPunchline = false;
};

showPunchline = () => {
    $("#punchline-box").animate({left: '0%'});

    showingPunchline = true;
};

horizontalHide = () => {
    $("#setup-box").animate({left: '100%'}, 400).css('left', '-100%');
    $("#punchline-box").animate({left: '-100%'}, 400).css('left', '100%');
};