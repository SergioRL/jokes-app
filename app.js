// Firstly we load all the ENV variables
const dotenv = require('dotenv');
dotenv.config({ path: './config/config.env' });

const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const path = require('path');
const db = require('./db/db');
const jokeImportService = require('./services/joke-import-service');


// App initializing
const app = express();
app.use(cors());

// Logging
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// DB initialization
db.initialize();

// Import Jokes Data
jokeImportService.importAllJokes();

// Routes
app.use('/api', require('./routes/api'))
app.use('/', require('./routes/index'))
app.use(express.static(path.join(__dirname, 'public')));

// Handlebars
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    extname: '.hbs',
    layouts: __dirname + '/views/layouts'}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', '.hbs');

const PORT = process.env.PORT || 5000

app.set('PORT', PORT);

app.listen( PORT, () => {
    console.log(`App is up! Listening at URL: http://localhost:${PORT}/`);
});


