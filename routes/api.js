const express = require('express');
const router = express.Router();
const jokeService = require('../services/joke-service');

//  @desc   Joke
//  @route  GET /
router.get('/joke/random', (req, res) =>{
    jokeService.getRandomJoke(
        (result) => {
            res.send(result);
        });
});


module.exports = router;