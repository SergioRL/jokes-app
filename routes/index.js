const express = require('express');
const router = express.Router();


//  @desc   Joke View/Landing
//  @route  GET
router.get('/', (req, res) =>{
    res.render('joke', {jokeSetup: 'Setup 1', jokePunchline: 'Punchline 1'});
});

//  @desc   Joke View
//  @route  GET
router.get('/view/joke', (req, res) =>{
    res.render('joke', {jokeSetup: 'Setup 1', jokePunchline: 'Punchline 1'});
});


module.exports = router;