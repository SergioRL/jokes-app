const request = require('request-promise');

module.exports.getJokesArray = function() {
    return request({json: true, url: process.env.JOKES_JSON_URL});
}